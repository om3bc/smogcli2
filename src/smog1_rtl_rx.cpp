/*
 * Copyright 2019-2021 Peter Horvath, Miklos Maroti, Joe Illes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "blocks.hpp"
#include "filter.hpp"
#include "predict.h"
#include "rtl_dongle.hpp"
#include "satellite.hpp"
#include "tle.hpp"
#include <unistd.h>
#include <atomic>
#include <cctype>
#include <chrono>
#include <csignal>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>

using namespace std;

#ifdef _WIN32
#include "getopt/getopt.h"
#endif

static std::atomic<bool> do_exit(false);

static const uint32_t TUNING_OFFSET = 55000;
static const uint32_t DEFAULT_SAMPLE_RATE = 1600000;
static const size_t DEFAULT_WORK_LENGTH = 2048;

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        std::cerr << "ERROR: from tcgetattr: " << strerror(errno) << std::endl;
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag &= (tcflag_t) ~PARENB; // Clear parity bit, disabling parity (most common)
    tty.c_cflag &= (tcflag_t) ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
    tty.c_cflag &= (tcflag_t) ~CSIZE; // Clear all bits that set the data size
    tty.c_cflag |= (tcflag_t) CS8; // 8 bits per byte (most common)
    tty.c_cflag &= (tcflag_t) ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
    tty.c_cflag |= (tcflag_t) CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

    tty.c_lflag &= (tcflag_t) ~ICANON;
    tty.c_lflag &= (tcflag_t) ~ECHO; // Disable echo
    tty.c_lflag &= (tcflag_t) ~ECHOE; // Disable erasure
    tty.c_lflag &= (tcflag_t) ~ECHONL; // Disable new-line echo
    tty.c_lflag &= (tcflag_t) ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
    tty.c_iflag &= (tcflag_t) ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
    tty.c_iflag &= (tcflag_t) ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

    tty.c_oflag &= (tcflag_t) ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
    tty.c_oflag &= (tcflag_t) ~ONLCR; // Prevent conversion of newline to carriage return/line feed

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        std::cerr << "ERROR: from tcsetattr: " << strerror(errno) << std::endl;
        return -1;
    }
    return 0;
}

void usage(void)
{
    std::cerr << "SMOG-1 recorder for RTL2832 based DVB-T receivers\n\n"
                 "Usage: smog1_rtl_rx [-options]\n"
                 "\t-d device_index (default: 0)\n"
                 "\t-T enable bias-T on GPIO PIN 0 (works for rtl-sdr.com v3 dongles)\n"
                 "\t-g tuner gain (default: 48dB auto=automatic, NOT RECOMMENDED)\n"
                 "\t-p kalibrate-sdr reported fractional ppm error (default: 0.0)\n"
                 "\t-F forced continuous recording, no Doppler correction\n"
                 "\t-i track the given satellite ID (default: 47964)\n"
                 "\t-f downlink frequency for sat (default: 437345000.0 Hz)\n"
                 "\t-k download TLE data from celestrak.com\n"
                 "\t-S use 2 Msps/62.5 ksps mode (default: 1.6 Msps/50 ksps)\n"
                 "\t-8 decimation by 8 (default: 32)\n"
                 "\t-O dump downconverted samples to STDOUT in binary cf32 format\n"
                 "\t-b disable the 1/128 rescaling of raw samples\n"
                 "\t-n download tle-new.txt instead of cubesat.txt from celestrak.com\n"
                 "\t-a download active.txt instead of cubesat.txt from celestrak.com\n"
                 "\t-t enable test messages (default: false)\n"
                 "\t-l enable azimuth/elevation logging (default: false)\n"
                 "\t-L enable GS232 compatible rotator (default: false)\n"
                 "\t-P enable rotator parking -P xxx (default: false)\n"
                 "\t-o disable antenna overturn +-25 deg (default: enabled)\n"
                 "\t-I enable flip - 180 degree elevation (default: false)\n"
                 "\t-N zero point: North (default: South)\n"
                 "\t-A serial port baudrate (default: 9600)\n"
                 "\t-s enable printing sat and signal statistics (default: false)\n"
                 "\t-B file buffer size, useful for slow SD cards (default: 10 Mb)\n"
                 "\t-e elevation limit for start recording (default: 0 deg)\n"
                 "\t-h prints this help message\n";
}

static void sighandler(int) { do_exit = true; }

void write_json(const Json::Value& root, const std::string& basename)
{
    std::ofstream metafile;
    metafile.open(basename + ".meta", std::ofstream::trunc);
    metafile << root;
    metafile.close();
}

int main(int argc, char* argv[])
{
    bool enable_biastee = false;
    bool update_tle = false;
    int dongle_gain = 480;
    float real_ppm_error = 0.0f;
    long sat1_id = 47964;
    double sat1_downlink = 437345000;
    bool dump_samples = false;
    const char* device = NULL;
    size_t file_buffer_size = 10 * 1024 * 1024;
    float elevation_limit = 0.0f;
    bool log_az_el = false;
    bool turn_az_el = false;
    bool print_statistics = false;
    uint32_t sample_rate = DEFAULT_SAMPLE_RATE;
    float scaling = 1.0f / 128.0f;
    std::string tle_file_name("cubesat.txt");
    std::ofstream azel_file_1;
    std::chrono::steady_clock::time_point start1;
    bool forced_mode = false;
    bool enable_test = false;
    bool enable_flip = false;
    bool parking = false;
    int parking_value = 180;
    uint32_t output_sample_rate = sample_rate / 32;
    int opt;
    char azel_file_2[30];
    int wlen;
    int baudrate = 9600;
    int port = -1;
    bool turning = false;
    const char* strgain = NULL;
    bool enable_overturn = true;
    bool zero_north = false;
    int dongle_count = 0;

    while ((opt = getopt(argc, argv, "f:d:g:p:Tki:Fe:B:OShbalsnLP:t8A:oNI")) != -1) {
        switch (opt) {
        case 'd':
            device = optarg;
            break;
        case 'g':
            strgain = optarg;
            if (!strcmp(strgain,"auto"))
                dongle_gain = -1;
            else
                dongle_gain = std::atof(strgain) * 10;
            break;
        case 'p':
            real_ppm_error = std::atof(optarg);
            break;
        case 'T':
            enable_biastee = true;
            break;
        case 't':
            enable_test = true;
            break;
        case 'k':
            update_tle = true;
            break;
        case 'F':
            forced_mode = true;
            break;
        case 'i':
            sat1_id = std::atol(optarg);
            break;
        case 'f':
            sat1_downlink = std::atof(optarg);
            break;
        case 'e':
            elevation_limit = std::atof(optarg);
            break;
        case 'O':
            dump_samples = true;
            break;
        case 'S':
            sample_rate = 2000000;
            break;
        case 'b':
            scaling = 1.0f;
            break;
        case 'a':
            tle_file_name = "active.txt";
            break;
        case 'n':
            tle_file_name = "tle-new.txt";
            break;
        case 'l':
            log_az_el = true;
            break;
        case 'L':
            turn_az_el = true;
            break;
        case 'o':
            enable_overturn = false;
            break;
        case 'N':
            zero_north = true;
            break;
        case 'I':
            enable_flip = true;
            break;
        case 'P':
            parking_value = std::atol(optarg);
            parking = true;
            break;
        case 's':
            print_statistics = true;
            break;
        case 'B':
            file_buffer_size =
                1024 * 1024 * static_cast<size_t>(std::max(std::atol(optarg), 0L));
            break;
        case '8':
            output_sample_rate = sample_rate / 8;
            break;
        case 'A':
            baudrate = std::atof(optarg);
            break;
        case 'h':
        default:
            usage();
            return 1;
        }
    }

    if (turn_az_el) {
        port = open("/dev/ttyUSB0",O_RDWR | ONLCR);
        if (port < 0)
            std::cerr << "ERROR: error opening serial port." << std::endl;
        else {
            switch (baudrate) {
                case 1200:
                    set_interface_attribs(port, B1200);
                    break;
                case 2400:
                    set_interface_attribs(port, B2400);
                    break;
                case 4800:
                    set_interface_attribs(port, B4800);
                    break;
                case 9600:
                    set_interface_attribs(port, B9600);
                    break;
                case 19200:
                    set_interface_attribs(port, B19200);
                    break;
                case 38400:
                    set_interface_attribs(port, B38400);
                    break;
                case 57600:
                    set_interface_attribs(port, B57600);
                    break;
                default:
                    std::cerr << "ERROR: Invalid serial port baudrate!" << std::endl
                              << "       Allowed: 1200, 2400, 4800, 9600, 19200, 38400, 57600" << std::endl
                              << "       Using default 9600Bd." << std::endl;
                    set_interface_attribs(port, B9600);
                    baudrate = 9600;
            }
            if (baudrate != 9600)
                std::cerr << "INFO: serial port baudrate: " << baudrate << "Bd." << std::endl;
        }
    }

    const uint32_t tuning_offset = output_sample_rate + 5000;
    uint32_t dongle_center_freq = std::round(sat1_downlink - tuning_offset);

    double real_center_freq =
        std::round(dongle_center_freq * (1.0 + real_ppm_error * 1e-6));

    sat_tracker tracker(
        { sat_tracker::sat_t(sat1_id, sat1_downlink) }, update_tle, tle_file_name);

    if (!tracker.is_trackable(0) && !forced_mode) {
        if (sat1_id == 0) {
            std::cerr << "ERROR: no satellite ID given." << std::endl;
        } else {
            std::cerr
                << "ERROR: no QTH file could be found and/or no TLE file could be found."
                << std::endl;
        }
        std::cerr << "ERROR: specify -F to force continuous recording with Doppler "
                     "correction disabled."
                  << std::endl;
        std::cerr << "ERROR: Exiting." << std::endl;
        return 1;
    }

    if (forced_mode) {
        std::cerr << "WARNING: Doppler correction disabled, using fixed RX frequency!"
                  << std::endl;
        if (log_az_el) {
            log_az_el = false;
            std::cerr << "WARNING: azimuth/elevation logging disabled" << std::endl;
        }
        if (turn_az_el) {
            turn_az_el = false;
            std::cerr << "WARNING: antenna rotator disabled" << std::endl;
        }
    }

    if (elevation_limit != 0.0f)
        std::cerr << "INFO: elevation limit is " << std::setprecision(1) << std::fixed
                  << elevation_limit << " deg" << std::endl;

    if (zero_north)
        std::cerr << "INFO: zero point is in North" << std::endl;

    if (enable_flip)
        std::cerr << "INFO: 180 degree elevation is enabled" << std::endl;

    if (!enable_overturn)
        std::cerr << "INFO: overturn disabled" << std::endl;

    buffer_t buffer(2, file_buffer_size);
    if (enable_test)
        std::cerr << "INFO: dongle buffer size: " << buffer.size()
                  << ", file buffer size: " << file_buffer_size << std::endl;

    rtl_dongle dongle(buffer, scaling, device, enable_test);
    if (!dongle.is_opened()) {
        const char* devi = device;
        if (device == NULL) devi = "0";
        std::cerr << "ERROR: device " << devi << " is not available " << std::endl;
        return 2;
    }
    dongle.set_sampling_rate(sample_rate);
    dongle.set_tuner_gain(dongle_gain);
    if (enable_biastee) dongle.set_bias_tee(enable_biastee);
    dongle.set_center_freq(dongle_center_freq,enable_test);

    Json::Value options;
    options["device"] = dongle.get_device_name();
    options["gain"] = dongle_gain;
    options["ppm_error"] = real_ppm_error;
    options["file_buffer"] = static_cast<unsigned int>(file_buffer_size);
    options["input_samp_rate"] = sample_rate;
    options["output_samp_rate"] = output_sample_rate;
    options["elevation_limit"] = elevation_limit;
    options["biastee"] = enable_biastee;

    if (sat1_downlink != 437345000)
        std::cerr << "INFO: sattelite downlink frequency is " << std::fixed
                  << std::setprecision(3) << (sat1_downlink * 1e-6) << " MHz" << std::endl;
    if (enable_test) {
        std::cerr << "INFO: real center frequency is " << std::setprecision(0) << std::fixed
                  << real_center_freq << " Hz" << std::endl;
        if (!print_statistics)
            std::cerr << "INFO: printing of satellite and signal statistic are disabled"
                      << std::endl;
    }

    sat_recorder recorder(
        buffer, 0, sample_rate, output_sample_rate, DEFAULT_WORK_LENGTH, file_buffer_size);
    sat_recorder dumper(
        buffer, 1, sample_rate, output_sample_rate, DEFAULT_WORK_LENGTH, file_buffer_size);
    
    dongle.start(enable_test);

    std::signal(SIGINT, sighandler);
    std::signal(SIGTERM, sighandler);
#ifndef _WIN32
    std::signal(SIGQUIT, sighandler);
    std::signal(SIGPIPE, sighandler);
#endif

    if (dump_samples)
        dumper.start("-");

    unsigned int delay_count = 0;
    unsigned int last_overruns = 0;
    float last_max_signal = 0.0;
    int last_max_signal_count = 0;
    int last_azimuth = 0;
    int last_elevation = 0;
    bool stop_turn = false;
    bool parked = true;

    recorder.set_correction((real_center_freq - sat1_downlink) / sample_rate);

    while (!do_exit) {
        if (++delay_count >= 100)
            delay_count = 0;

        if (!forced_mode)
            tracker.calculate(print_statistics && (delay_count == 0),elevation_limit,zero_north,enable_flip);

        // this will be overwritten if the satellite is above
        float dumper_correction = (real_center_freq - sat1_downlink) / sample_rate;

        if (tracker.is_trackable(0) && sat1_downlink > 0 && !forced_mode) {
            float correction =
                (real_center_freq - sat1_downlink - tracker.get_doppler(0)) / sample_rate;
            recorder.set_correction(correction);
            if (tracker.get_elevation(0) >= elevation_limit)
                dumper_correction = correction;
        }

        double elevation = tracker.get_elevation(0);
        if (elevation > 90.0) elevation = 180.0 - elevation;
        if (!recorder.is_running() && (elevation >= elevation_limit || forced_mode)) {
            std::string basename;
            if (!forced_mode)
                basename = tracker.get_filename(0, device);
            else {
                if (tracker.is_trackable(0))
                    basename = tracker.get_name(0);
                else
                    basename = "SCN" + std::to_string(sat1_id);

                basename += "_";
                basename += std::to_string(sat_tracker::current_daynum());
            }

            if (log_az_el) {
                azel_file_1.open(basename + ".plog");
                start1 = std::chrono::steady_clock::now();
            }
            recorder.start(basename.c_str());

            if (!forced_mode) {
                Json::Value root;
                root["options"] = options;
                tracker.add_json_data(0, root);
                write_json(root, basename);
            }
        } else if (recorder.is_running()) {
            if (!forced_mode && elevation < elevation_limit) {
                recorder.stop();
                if (log_az_el)
                    azel_file_1.close();
                if (turn_az_el) {
                    stop_turn = true;
                    if (parking) {
                        sprintf(azel_file_2,"W%03d 000\r\n", parking_value);
                        if (port >= 0) write(port,azel_file_2, 10);
                        parked = true;
                    }

                }
            }
        }

        if (recorder.is_running() && (delay_count % 50 == 0) && log_az_el) {
            std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
            int sample_offset_cnt =
                output_sample_rate / 1000 *
                std::chrono::duration_cast<std::chrono::milliseconds>(now - start1)
                    .count();
            azel_file_1 << sample_offset_cnt << " " << std::fixed << std::setprecision(1)
                        << tracker.get_azimuth(0) << " " << tracker.get_elevation(0)
                        << " " << int(tracker.get_range(0)) << std::endl;
        }

        if (delay_count == 0 && turn_az_el) {
            if (elevation < (elevation_limit - 15.0)) turning = false;
            if (tracker.get_direction(0) != 0 && turning == false && enable_overturn) {
                sprintf(azel_file_2,"%c\r\n",tracker.get_direction(0));
                wlen = write(port,azel_file_2, 3);
                turning = true;
            }
            if (elevation >= (elevation_limit - 7.0) && tracker.get_above(0) == true) {
                int int_elevation = 0;
                if (tracker.get_elevation(0) > 90.0) int_elevation = 180;
                int int_azimuth = std::round(tracker.get_azimuth(0));
                if (int_azimuth == 360) int_azimuth = 0;
                if (tracker.get_elevation(0) > 0.0 && tracker.get_elevation(0) < 180.0)
                    int_elevation = std::round(tracker.get_elevation(0));
                if ((last_azimuth != int_azimuth || last_elevation != int_elevation) && !stop_turn) {
                    last_azimuth = int_azimuth;
                    last_elevation = int_elevation;
                    sprintf(azel_file_2,"W%03d %03d\r\n", int_azimuth, int_elevation);
                    if (port >= 0) {
                        wlen = write(port,azel_file_2, 10);
                        if (wlen < 0)
                            std::cerr << "ERROR: serial port is not available." << std::endl;
                    }
                    parked = false;
                }
            } else {
                if (turn_az_el && parking && !parked) {
                    parked = true;
                    sprintf(azel_file_2,"W%03d 000\r\n", parking_value);
                    if (port >= 0) write(port,azel_file_2, 10);
                }
                stop_turn = false;
            }
        }

        dumper.set_correction(dumper_correction);

        if (delay_count == 0) {
            std::array<float, 2> stat = dongle.get_statistics();
            unsigned int overruns = dongle.get_overruns();

            if (print_statistics) {
                std::stringstream msg;
                msg << "INFO: signal amplitude maximum: " << std::setprecision(5)
                    << std::fixed << stat[0] << ", average: " << stat[1]
                    << ", total overruns: " << overruns << std::endl;
                std::cerr << msg.str();
            } else {
                if (overruns != last_overruns) {
                    std::stringstream msg;
                    msg << "WARNING: total overruns: " << overruns << std::endl;
                    std::cerr << msg.str();
                    last_overruns = overruns;
                }
                if (stat[0] != last_max_signal) {
                    last_max_signal = stat[0];
                    last_max_signal_count = 0;
                    dongle_count = 0;
                } else if (++last_max_signal_count >= 50) {
                    std::stringstream msg;
                    msg << "WARNING: signal amplitude is permanently: "
                        << std::setprecision(5) << std::fixed << stat[0] << ", ";
                    if (stat[0] <= 0.0) {
                        msg << "check the rtl dongle";
                        if (++dongle_count >= 3) do_exit = true;
                    }
                    else if (stat[0] >= 1.0)
                        msg << "reduce the gain";
                    else
                        msg << "check the antenna";
                    msg << std::endl;
                    std::cerr << msg.str();
                    last_max_signal_count = 0;
                }
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (do_exit) {
        if (dongle_count < 3) {
            std::cerr << "INFO: aborted by signal" << std::endl;
        } else {
            std::cerr << "ERROR: Dongle error detected" << std::endl;
        }
    }
    if (recorder.is_running())
        recorder.stop();

    if (dumper.is_running())
        dumper.stop();

    if (port >= 0) {
        if (parking) {
            sprintf(azel_file_2,"W%03d 000\r\n", parking_value);
            write(port,azel_file_2, 10);
        }
        close(port);
    }

    dongle.stop();

    return 0;
}
