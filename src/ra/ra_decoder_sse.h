/* Copyright (C) Miklos Maroti 2015-2016 */

#ifndef __RA_DECODER_SSE_H__
#define __RA_DECODER_SSE_H__

#include "ra_config.h"

#ifdef __cplusplus
extern "C" {
#endif

void ra_decoder_sse(const float* softbits, uint16_t* packet, int passes, float scaling);

#ifdef __cplusplus
}
#endif

#endif //__RA_DECODER_SSE_H__
