#!/bin/bash

while getopts "d:" option
do
case "${option}"
in
d) DAY=${OPTARG};;
esac
done

cd $HOME/gsc
date '+%Y-%m-%d %H:%M:%S'
echo "Start decode cf32 files"
#./smog1_decode -p *$DAY*.cf32
./smog1_decode -t 0 -s -1 -d -3 -p -l 1000 *$DAY*.cf32
echo "Start upload"
date '+%Y-%m-%d %H:%M:%S'
sleep 1
python3 ./python/smog1_upload.py *$DAY*.pkts
echo "Moving files to archive after 10 sec."
read -t 10 -n 1 -e -p "Move? [Y/n]" yn
case $yn in
	[Nn]* ) echo "Exit"; cd $HOME/gsc;;
	* ) echo "Moving";
            mv *$DAY*.pkts ./OLD_pkts
            mv *$DAY*.cf32 ./OLD_cf32
            mv *$DAY*.meta ./OLD_cf32
        ;;
esac
echo "Process finished"

