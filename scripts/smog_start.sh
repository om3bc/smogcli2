#!/bin/bash

#sleep 1
#screen -S rotor -d -m
#screen -r rotor -X stuff "
#cd /home/$USER/Doppler/
#python doppler_cli.py
#"
echo "----------------------------------"
date '+%Y-%m-%d %H:%M:%S'
a=`ps aux | grep smog1_rtl_rx | grep -vi -e bash -e bin\/sh -e grep -e screen`
#a=`ps aux | grep smog1_rtl_rx`
#echo $a
if [ -n "$a" ]
then
	echo "smog1_rtl_rx already running."
	exit
else
	echo "Starting smog."
	killall screen
fi

echo "smog1_rtl_rx dev0 start up as smog0"
sleep 1
screen -S smog0 -d -m
screen -r smog0 -X stuff "
cd /home/pi/gsc/
./smog1_rtl_rx -p 1.2 -d 0 -e 0.6 -k -L
"
echo "smog1_rtl_rx dev1 start up as smog1"
sleep 1
screen -S smog1 -d -m
screen -r smog1 -X stuff "
cd /home/pi/gsc/
./smog1_rtl_rx -p 1.2 -d 1 -e 0.7
"
#echo "done------------------------------"

exit 0

